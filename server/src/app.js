const express = require('express');
const dotenv = require('dotenv');
const path= require('path');//static website
const bcrypt=require('bcryptjs');
const jwt=require('jsonwebtoken');
const cookieParser=require('cookie-parser');
const auth=require('./middleware/auth');


const hbs= require('hbs');
const app= express();
require("./db/conn");
const Register= require("./models/registers");
//const { response } = require('express');

const port= process.env.PORT || 8000;
const static_path=path.join(__dirname,"../public");//static website
const template_path=path.join(__dirname,"../templates/views");//dynamic site
const partials_path=path.join(__dirname,"../templates/partials");//dynamic site
app.use(express.json());
app.use(cookieParser());
app.use(express.urlencoded({extended:false}));

app.use(express.static(static_path));//static website
app.set('view engine','hbs');
app.set("views",template_path);//static website
hbs.registerPartials(partials_path);
app.get('/',(req,res)=>{
    res.render('index');
})
app.get('/secret',auth,(req,res)=>{
    //console.log(`This is the cookie awsome ${req.cookies.jwt}`);
    res.render('secret');
});
app.get('/logout',auth,async(req,res)=>{
    try{
        //remove cookie from db
        //for single logout
    // req.user.tokens=req.user.tokens.filter((currentElement)=>{
    //     return currentElement.token===req.token
    // })
    //logout from all devices
    req.user.tokens=[];

//remove cookie from browser
        res.clearCookie("jwt");
     console.log("logout successfully");
      await req.user.save();
     res.render("login");
    }catch(err){
        res.status(500).send(err);
    }
    
});
app.get('/register',(req,res)=>{
    res.render('register');
});

//create a new user in our data base
app.post('/register',async(req,res)=>{
    try{
       // console.log(req.body.firstname);
        //req.send(req.body.firstname);
        const password=req.body.password;
        const cpassword=req.body.cpassword;
        if(password===cpassword){
            const registerEmployee = new Register({
                firstname:req.body.firstname,
                lastname:req.body.lastname,
                email:req.body.email,
                gender:req.body.gender,
                phone:req.body.phone,
                age:req.body.age,
                password:req.body.password,
                cpassword:req.body.cpassword
                
            })
            //middleware for jwt
            const token= await registerEmployee.generateAuthToken();
            res.cookie("jwt",token,{
                expires:new Date(Date.now() + 80000),
                httpOnly:true
            });
          const registered= await registerEmployee.save();
            res.status(201).render('index');

        }else{
            res.send('Passwords are not matching');
        }

    }catch(error){
        res.status(400).send(err);
    }
});
//login
app.get('/login',(req,res)=>{
    res.render('login');
})

app.post('/login',async(req,res)=>{
    try{
       // console.log(req.body.firstname);
        //req.send(req.body.firstname);
        const email=req.body.email;
        const password=req.body.password;
        const useremail= await Register.findOne({email:email});
        const isMatch= await bcrypt.compare(password,useremail.password);
        const token= await useremail.generateAuthToken();
        console.log("the token part" + token);
        res.cookie("jwt",token,{
            expires:new Date(Date.now() + 80000),
            httpOnly:true
            //secure:true
        });
        if(isMatch){
            res.status(201).render('index');
        }else{
            res.send('Invalid email and password');
        }

    }catch(error){
        res.status(400).send(err);
    }
});


app.listen(port,()=>{
    console.log(`The server is running at ${port}`);
})