const bcrypt = require('bcryptjs');
const jwt=require('jsonwebtoken');
const mongoose=require('mongoose');
const employeeSchema= new mongoose.Schema({
    firstname:{
        type:String,
        required:false

    },
    lastname:{
        type:String,
        required:false

    },      
    
   
    email:{
        type:String,
        required:false,
        unique:false

    },
    gender:{
        type:String,
        required:false

    },
    phone:{
        type:Number,
        required:false,
        unique:false

    },
    age:{
        type:Number,
        required:false,
        
    },
    password:{
        type:String,
        required:false
        
    },
    cpassword:{
        type:String,
        required:false        
    }, 
    tokens:[{
        token:{
            type:String,
            required:true
        }
    }]
   });
   //token coding
   employeeSchema.methods.generateAuthToken= async function(){
    try{
        
        const token=jwt.sign({_id:this._id.toString()},"abcdyhjklmbgdghjkrsgfbhoghkgsbvt");
        this.tokens=this.tokens.concat({token:token});
        await this.save();
        return token;

    }catch(err){
        res.send("the err part "+ err);
        console.log("the err part" + err);

    }
   }
   //Here we do password hashing
   employeeSchema.pre("save", async function(next){
    if(this.isModified("password")){
        
    this.password= await bcrypt.hash(this.password,10);
    this.cpassword= await bcrypt.hash(this.cpassword,10);

    }
    
    next();
   })
//Now we need to create a collection
const Register = new mongoose.model("Register",employeeSchema);
module.exports=Register;